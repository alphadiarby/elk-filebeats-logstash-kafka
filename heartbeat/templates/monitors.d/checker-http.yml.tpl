{% for var in url_http_check %}
- type: http
  name: "{{ var.name }}"
  schedule: '@every 6s'
  urls: ["{{ var.url }}"]
  check.response.status: 200

{% endfor %}