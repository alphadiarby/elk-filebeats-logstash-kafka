curl -X POST "http://10.228.220.181:5601/api/alerting/rule  -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d'
{
  "params":{
      "aggType":"avg",
      "termSize":6,
      "thresholdComparator":">",
      "timeWindowSize":5,
      "timeWindowUnit":"m",
      "groupBy":"top",
      "threshold":[
         1000
      ],
      "index":[
         ".test-index"
      ],
      "timeField":"@timestamp",
      "aggField":"sheet.version",
      "termField":"name.keyword"
   },
   "consumer":"alerts",
   "rule_type_id":".index-threshold",
   "schedule":{
      "interval":"1m"
   },
   "actions":[
      {
         "id":"dceeb5d0-6b41-11eb-802b-85b0c1bc8ba2",
         "group":"threshold met",
         "params":{
            "level":"info",
            "message":"alert \u0027{{alertName}}\u0027 is active for group \u0027{{context.group}}\u0027:\n\n- Value: {{context.value}}\n- Conditions Met: {{context.conditions}} over {{params.timeWindowSize}}{{params.timeWindowUnit}}\n- Timestamp: {{context.date}}"
         }
      }
   ],
   "tags":[
      "cpu"
   ],
   "notify_when":"onActionGroupChange",
   "name":"my alert"
}\u0027
'
#### pour l'apikey on ajoute -H 'Authorization: ApiKey aVZlLUMzSUJuYndxdDJvN0k1bU46aGxlYUpNS2lTa2FKeVZua1FnY1VEdw=='
